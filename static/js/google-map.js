function initMap() {
  // The location of Uluru
  var beefitLoc = {lat: 7.1037822, lng: 125.6438701};
  // The map, centered at beefitLoc
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 19, center: beefitLoc});
  // The marker, positioned at beefitLoc
  var marker = new google.maps.Marker({position: beefitLoc, map: map});
}
