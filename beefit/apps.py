from django.apps import AppConfig


class BeefitConfig(AppConfig):
    name = 'beefit'
