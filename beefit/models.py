from django.db import models
from djrichtextfield.models import RichTextField

TIME_CHOICES = (
    (1, 'Month'),
    (2, '3 Months'),
    (3, '6 Months'),
    (4, 'Year'),
)


class Page(models.Model):
    header_text = models.CharField(max_length=150)
    header_subtext = models.CharField(max_length=50)
    footer_about = models.TextField(max_length=300)
    header_picture = models.ImageField(upload_to='headers')
    process_description = models.TextField(max_length=300, null=True, blank=True)
    programs_description = models.TextField(max_length=300, null=True, blank=True)
    packages_description = models.TextField(max_length=300, null=True, blank=True)
    location_description = models.TextField(max_length=300, null=True, blank=True)
    is_active = models.BooleanField(default=True)


class Contact(models.Model):
    number = models.CharField(max_length=15)
    is_active = models.BooleanField(default=True)
    page = models.ForeignKey('Page', on_delete=models.CASCADE)


class Process(models.Model):
    title = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class Program(models.Model):
    title = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class Pricing(models.Model):
    amount = models.FloatField()
    time_span = models.PositiveSmallIntegerField(choices=TIME_CHOICES)

    def __str__(self):
        time_span = dict(TIME_CHOICES)
        return "{} for {}".format(self.amount, time_span[self.time_span])


class Package(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='packages')
    is_active = models.BooleanField(default=True)
    pricing_plans = models.ManyToManyField('Pricing')

    def __str__(self):
        return self.title


class Coach(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    photo = models.ImageField(upload_to='coaches')
    social_insta = models.CharField(max_length=200, null=True, blank=True)
    social_facebook = models.CharField(max_length=200, null=True, blank=True)
    social_twitter = models.CharField(max_length=200, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Photo(models.Model):
    description = models.CharField(max_length=255, null=True, blank=True)
    photo = models.ImageField(upload_to='photos')

    def __str__(self):
        return self.description


class Rule(models.Model):
    title = models.CharField(max_length=225, null=True, blank=True)
    rules = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title