from django.shortcuts import render
from django.views.generic.base import TemplateView

from beefit.models import *


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page'] = Page.objects.filter(is_active=True).last()
        context['processes'] = Process.objects.all()
        context['programs'] = Program.objects.all()[:3]
        context['coaches'] = Coach.objects.filter(is_active=True)
        context['packages'] = Package.objects.filter(is_active=True)
        context['gallery'] = Photo.objects.all()[:4]
        return context


class ProgramsView(TemplateView):
    template_name = 'programs.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['programs'] = Program.objects.all()
        return context


class PhotosView(TemplateView):
    template_name = 'photos.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['photos'] = Photo.objects.all()
        context['page'] = Page.objects.filter(is_active=True).last()
        return context


class RulesView(TemplateView):
    template_name = 'rules.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rules'] = Rule.objects.all()
        context['page'] = Page.objects.filter(is_active=True).last()
        return context
