from django.core.management.base import BaseCommand, CommandError

from beefit.models import *

FILLER_TEXT = ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi augue '
               'mi, lobortis accumsan mi. Sed vitae vestibulum purus.')


class Command(BaseCommand):
    help = 'Creates initial data'

    def handle(self, *args, **options):
        try:
            # Initialize Page
            page = Page.objects.create(
                header_text='Do something today that your future self will thank you for',
                header_subtext='BeeFit Gym Plus',
                footer_about='BeeFit is a physical fitness center ready to serve you',
                header_picture='headers/index.jpg',
                process_description=('"Be exactly who you are. You can fit in any space you '
                                     'see yourself in. Be fearless." -Dawn Richard'),
                programs_description='We offer programs that fits according to your personal goals.',
                packages_description='Stay fit and avail our packages.',
                location_description='We are located at beside Azuela Cove Davao City.',
            )

            # Initialize Contact
            Contact.objects.create(
                number='+63 82 305 2701',
                page=page,
            )

            # Initialize Process
            processes_data = [
                {'title': 'Analyze Your Goal', 'icon': 'ruler'},
                {'title': 'Work Hard On It', 'icon': 'gym'},
                {'title': 'Improve', 'icon': 'tools-and-utensils'},
                {'title': 'Achieve', 'icon': 'abs'},
            ]
            for item in processes_data:
                Process.objects.create(
                    title=item['title'],
                    icon=item['icon'],
                    description=FILLER_TEXT,
                )

            # Initialize Program
            programs_data = [
                {'title': 'Aerobics', 'icon': 'heart'},
                {'title': 'Weight Lifting', 'icon': 'workout'},
                {'title': 'MetaFit', 'icon': 'running'},
            ]
            for item in programs_data:
                Program.objects.create(
                    title=item['title'],
                    icon=item['icon'],
                    description=FILLER_TEXT,
                )

            # Initialize Pricing
            thousand_per_month = Pricing.objects.create(
                amount=1000,
                time_span=1,
            )

            # Initialize Package
            package_titles = ['Gym Usage', 'Aerobics', 'MetaFit']
            for i, title in enumerate(package_titles):
                package = Package.objects.create(
                    title=title,
                    description=FILLER_TEXT,
                    photo='packages/program-{}.jpg'.format(i + 1),
                )
                package.pricing_plans.add(thousand_per_month.id)

            # Initialize Coach
            for i in range(1, 7):  # Loop 1-6
                Coach.objects.create(
                    first_name='John',
                    last_name='Doe',
                    position='Owner / Head Coach',
                    description=FILLER_TEXT,
                    photo='coaches/trainer-{}.jpg'.format(i),
                    social_insta='cholo.recto',
                    social_facebook='cholo.recto',
                    social_twitter='cholo.recto',
                )

            # Initialize Photo (Gallery)
            for i in range(1, 5):  # Loop 1-4
                Photo.objects.create(
                    description='Photo {}'.format(i),
                    photo='photos/gallery-{}.jpg'.format(i),
                )

            self.stdout.write(self.style.SUCCESS('Successfully added initial data'))
        except Exception as err:
            raise CommandError(err)
