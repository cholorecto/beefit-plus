import base64

from django.core.files.base import ContentFile
from rest_framework import serializers

from beefit.models import *


class Base64ImageField(serializers.ImageField):
    """Custom image serializer for images in Base64 format"""
    def from_native(self, data):
        if isinstance(data, basestring) and data.startswith('data:image'):
            # base64 encoded image - decode
            format, imgstr = data.split(';base64,')  # format ~= data:image/X,
            ext = format.split('/')[-1]  # guess file extension

            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)

        return super(Base64ImageField, self).from_native(data)


class PageSerializer(serializers.ModelSerializer):
    header_picture = Base64ImageField(required=True, use_url=True)

    class Meta:
        model = Page
        fields = '__all__'


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'


class ProcessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Process
        fields = '__all__'


class PricingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pricing
        fields = '__all__'


class PackageSerializer(serializers.ModelSerializer):
    photo = Base64ImageField(required=True, use_url=True)

    class Meta:
        model = Package
        fields = '__all__'


class CoachSerializer(serializers.ModelSerializer):
    photo = Base64ImageField(required=True, use_url=True)

    class Meta:
        model = Coach
        fields = '__all__'
