from django.urls import include, path, re_path
from rest_framework.routers import DefaultRouter

from beefit.api import *
from beefit.views import IndexView, ProgramsView, PhotosView, RulesView


router = DefaultRouter()
router.register(r'pages', PageViewSet)
router.register(r'contacts', ContactViewSet)
router.register(r'processes', ProcessViewSet)
router.register(r'pricings', PricingViewSet)
router.register(r'packages', PackageViewSet)
router.register(r'coaches', CoachViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('programs/', ProgramsView.as_view()),
    path('photos/', PhotosView.as_view()),
    path('rules/', RulesView.as_view()),
    re_path(r'(?P<path>.*)', IndexView.as_view()),
]
