# from rest_framework.authentication import SessionAuthentication
# from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from beefit.models import *
from beefit.serializers import *


class PageViewSet(ModelViewSet):
    model = Page
    queryset = Page.objects.all()
    serializer_class = PageSerializer


class ContactViewSet(ModelViewSet):
    model = Contact
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


class ProcessViewSet(ModelViewSet):
    model = Process
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer


class PricingViewSet(ModelViewSet):
    model = Pricing
    queryset = Pricing.objects.all()
    serializer_class = PricingSerializer


class PackageViewSet(ModelViewSet):
    model = Package
    queryset = Package.objects.all()
    serializer_class = PackageSerializer


class CoachViewSet(ModelViewSet):
    model = Coach
    queryset = Coach.objects.all()
    serializer_class = CoachSerializer
